from django.conf.urls import url, include
from django.conf import settings

from apps.main.router import urls as main_urls

urlpatterns = [

    # MAIN
    url(r'^',        include(main_urls)),

]

if settings.DEBUG:
    urlpatterns += [
        # REST FRAMEWORK
        url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    ]

from rest_framework import routers
from django.conf.urls import include, url

from apps.main.api import UserViewSet
from apps.main.api import LoginView, LogoutView

router = routers.SimpleRouter()

router.register(r'users',                   UserViewSet,                    base_name='users')


urls = [

    # Authentication Urls
    url(r'^auth/login/$',           LoginView.as_view(),                    name='login'),
    url(r'^auth/logout/$',          LogoutView.as_view(),                   name='logout'),

    # Router Urls
    url(r'^',           include(router.urls)),
]

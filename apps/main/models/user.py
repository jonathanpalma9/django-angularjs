from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone


class UserManager(BaseUserManager):

    def create_user(self, email=None, password=None, **kwargs):
        if not email:
            raise ValueError('Users must have a valid email address.')

        if not kwargs.get('username'):
            raise ValueError('Users must have a valid username.')

        user = self.model(
            email=self.normalize_email(email), username=kwargs.get('username')
        )

        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, email, password, **kwargs):
        user = self.create_user(email, password, **kwargs)

        user.is_admin = True
        user.save()

        return user


class User(AbstractBaseUser):

    # TODO: Review user data
    email = models.EmailField(verbose_name=_('email address'), unique=True)
    username = models.CharField(verbose_name=_('username'), max_length=40, unique=True,
                                error_messages={
                                    'unique': _('A user with that username already exists.')
                                })

    first_name = models.CharField(verbose_name=_('first name'), max_length=40, blank=True)
    last_name = models.CharField(verbose_name=_('last name'), max_length=40, blank=True)

    is_active = models.BooleanField(verbose_name=_('active'), default=True)
    is_admin = models.BooleanField(verbose_name=_('is admin'), default=False)
    date_joined = models.DateTimeField(verbose_name=_('date joined'), default=timezone.now)
    last_update = models.DateTimeField(verbose_name=_('last update'), auto_now=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    class Meta:
        db_table = 'user'
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

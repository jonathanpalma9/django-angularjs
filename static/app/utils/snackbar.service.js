(function ($, _) {
    'use strict';

    angular
        .module('app.utils.services')
        .factory('SnackBar', SnackBar);


    function SnackBar() {
        /**
         * @name Snackbar
         * @desc The factory to be returned
         */
        return {
            error: error,
            show: show
        };

        ////////////////////

        /**
         * @name _snackbar
         * @desc Display a snackbar
         * @param {string} content The content of the snackbar
         * @param {Object} options Options for displaying the snackbar
         */
        function _snackbar(content, options) {
            options = _.extend({timeout: 3000}, options);
            options.content = content;

            $.snackbar(options);
        }


        /**
         * @name error
         * @desc Display an error snackbar
         * @param {string} content The content of the snackbar
         * @param {Object} options Options for displaying the snackbar
         */
        function error(content, options) {
            _snackbar('Error: ' + content, options);
        }


        /**
         * @name show
         * @desc Display a standard snackbar
         * @param {string} content The content of the snackbar
         * @param {Object} options Options for displaying the snackbar
         */
        function show(content, options) {
            _snackbar(content, options);
        }
    }
})($, _);

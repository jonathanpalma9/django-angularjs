(function () {
    'use strict';

    angular
        .module('app.utils.services')
        .constant('API_ROOT', '/api/v1/');
})();


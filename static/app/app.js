(function () {
    'use strict';

    angular
        .module('app', [
            // Dependencies
            'ngMaterial',

            // Apps
            'app.config',
            'app.routes',
            'app.utils',
            'app.main',
            'app.layout'
        ])
        .run(run);

    angular.module('app.config', []);
    angular.module('app.routes', ['ngRoute']);

    function run($http) {
        // Add validation to CSRF
        $http.defaults.xsrfHeaderName = 'X-CSRFToken';
        $http.defaults.xsrfCookieName = 'csrftoken';

        // Initialize material library
        $.material.init();
    }

})();
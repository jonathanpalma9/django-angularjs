(function () {
    'use strict';

    angular
        .module('app.layout.controllers')
        .controller('NavBarController', NavBarController);

    NavBarController.$inject = ['$scope', 'Authentication'];

    function NavBarController($scope, Authentication) {
        var ctrl = this;

        ctrl.logout = logout;

        function logout() {
            Authentication.logout();
        }
    }
})();

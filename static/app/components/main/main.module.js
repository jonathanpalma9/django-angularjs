(function () {
    'use strict';

    angular
        .module('app.main', [
            'app.main.controllers',
            'app.main.services'
        ]);

    angular.module('app.main.controllers', []);
    angular.module('app.main.services', ['ngCookies']);

})();

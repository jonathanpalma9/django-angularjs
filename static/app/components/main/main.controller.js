(function () {
    'use strict';

    angular
        .module('app.main.controllers')
        .controller('RegisterController', RegisterController)
        .controller('LoginController', LoginController);

    RegisterController.$inject = ['$location', '$scope', 'Authentication'];
    LoginController.$inject = ['$location', '$scope', 'Authentication'];

    function RegisterController($location, $scope, Authentication) {
        var user = this;

        user.register = register;

        function register() {
            Authentication.register(user.email, user.password, user.username);
        }
    }

    function LoginController($location, $scope, Authentication) {
        var user = this;

        user.login = login;

        activate();

        function activate() {
            if (Authentication.isAuthenticated()) {
                $location.url('/');
            }
        }

        function login() {
            Authentication.login(user.email, user.password);
        }
    }

})();

(function () {

    'use strict';

    angular
        .module('app.main.services')
        .factory('Authentication', Authentication);

    Authentication.$inject = ['$cookies', '$http', 'SnackBar', 'API_ROOT'];

    function Authentication($cookies, $http, SnackBar, API_ROOT) {

        var authenticatedUser = 'authenticatedUser';

        /**
         * @name Authentication
         * @desc The Factory to be returned
         */
        var Authentication = {
            login: login,
            logout: logout,
            register: register,
            getAuthenticatedUser: getAuthenticatedUser,
            isAuthenticated: isAuthenticated,
            setAuthenticatedUser: setAuthenticatedUser,
            unauthenticate: unauthenticate
        };

        return Authentication;

        function register(email, password, username) {
            return $http.post(API_ROOT + 'users/' , {
                username: username,
                password: password,
                email: email
            }).then(registerSuccessFn, registerErrorFn);

            function registerSuccessFn(data, status, headers, config) {
                Authentication.login(email, password);
            }

            function registerErrorFn(data, status, headers, config) {
                console.error('Authenticated:register -> ', data, status, headers, config);
                SnackBar.error(data.data.message);
            }
        }


        function login(email, password) {
            return $http.post(API_ROOT + 'auth/login/', {
                email: email, password: password
            }).then(loginSuccessFn, loginErrorFn);

            function loginSuccessFn(data, status, headers, config) {
                Authentication.setAuthenticatedUser(data.data);
                window.location = '/';
            }

            function loginErrorFn(data, status, headers, config) {
                console.error('Authenticated:login -> ', data, status, headers, config);
                SnackBar.error(data.data.message);
            }
        }

        function logout() {
            return $http.post(API_ROOT + 'auth/logout/', {})
                .then(logoutSuccessFn, logoutErrorFn);

            function logoutSuccessFn(data, status, headers, config) {
                Authentication.unauthenticate();
                window.location = '/';
            }

            function logoutErrorFn(data, status, headers, config) {
                console.error('Authenticated:logout -> ', data, status, headers, config);
            }
        }

        function getAuthenticatedUser() {
            if (!$cookies.get(authenticatedUser)) {
                return;
            }
            return JSON.parse($cookies.get(authenticatedUser));
        }

        function isAuthenticated() {
            return !!$cookies.get(authenticatedUser);
        }

        function setAuthenticatedUser(user) {
            $cookies.put(authenticatedUser, JSON.stringify(user));
        }

        function unauthenticate() {
            $cookies.remove(authenticatedUser);
        }
    }
})();
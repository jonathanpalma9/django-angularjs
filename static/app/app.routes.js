(function () {
    'use strict';

    angular
        .module('app.routes')
        .config(config);

    config.$inject = ['$routeProvider'];

    function config($routeProvider) {

        $routeProvider.when('/register', {
            controller: 'RegisterController',
            controllerAs: 'user',
            templateUrl: '/static/templates/main/register.html'
        }).when('/login', {
            controller: 'LoginController',
            controllerAs: 'user',
            templateUrl: '/static/templates/main/login.html'
        }).otherwise({
            redirectTo: '/'
        });
    }

})();
